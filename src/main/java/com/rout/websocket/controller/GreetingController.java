package com.rout.websocket.controller;

import com.rout.websocket.domain.Message;
import com.rout.websocket.domain.Response;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
public class GreetingController {

    @MessageMapping("/hello")
    @SendTo("/message/response")
    public Response respond(Message message) throws Exception{
        Thread.sleep(4000); // Note :  Just to delay the operation
        return new Response("Guten Morgen Sagar");
    }
}
